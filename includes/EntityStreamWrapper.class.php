<?php
/**
 * @file
 * Stream wrapper for url's to main video's.
 */

/**
 * Drupal stream wrapper base class for local files.
 *
 * This class provides a complete stream wrapper implementation. URIs such as
 * "public://example.txt" are expanded to a normal filesystem path such as
 * "sites/default/files/example.txt" and then PHP filesystem functions are
 * invoked.
 *
 * DrupalLocalStreamWrapper implementations need to implement at least the
 * getDirectoryPath() and getExternalUrl() methods.
 */
class EntityStreamWrapper implements DrupalStreamWrapperInterface {

  /**
   * Stream context resource.
   *
   * @var Resource
   */
  public $context;

  /**
   * A generic resource handle.
   *
   * @var Resource
   */
  public $handle = NULL;

  /**
   * Instance URI (stream).
   *
   * A stream is referenced as "scheme://target".
   *
   * @var String
   */
  protected $uri;

  /**
   * Instance external URL.
   *
   * The "real" external url of the current instance uri.
   *
   * @var String
   */
  protected $externalUrl;

  /**
   * The current stream wrapper of the current external uri.
   *
   * @var [type]
   */
  protected $wrapper;

  /**
   * Base implementation of setUri().
   */
  public function setUri($uri) {
    $this->uri = $uri;
    $this->externalUrl = NULL;
  }

  /**
   * Base implementation of getUri().
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * Set uri with fallback to current.
   *
   * @param string &$uri
   *   The uri after the operation.
   */
  public function ensureUri(&$uri) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
  }

  /**
   * Get external URL.
   *
   * @return string
   *   The external URL.
   */
  public function getExternalUrl() {
    return isset($this->externalUrl) ? $this->externalUrl : $this->externalUrl = static::generateExternalUrl($this->uri);
  }

  /**
   * Get context by uri.
   *
   * @return object|bool
   *   Stream wrapper instance on success, FALSE on failure.
   */
  public function getWrapper() {
    return isset($this->wrapper) ? $this->wrapper : $this->wrapper = file_stream_wrapper_get_instance_by_uri($this->getExternalUrl());
  }

  /**
   * Get context by uri.
   *
   * @param string &$uri
   *   Uri to get context by. Will be updated with the destination uri.
   *
   * @return object|bool
   *   Stream wrapper instance on success, FALSE on failure.
   */
  public function getWrapperByUri(&$uri) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    $uri = static::generateExternalUrl($uri);
    return file_stream_wrapper_get_instance_by_uri($uri);
  }

  /**
   * Base implementation of getMimeType().
   */
  public static function getMimeType($uri, $mapping = NULL) {
    $uri = static::generateExternalUrl($uri);
    $context = $uri ? file_stream_wrapper_get_instance_by_uri($uri) : FALSE;
    return $context ? $context->getMimeType($uri, $mapping) : FALSE;
  }

  /**
   * Support for chmod().
   */
  public function chmod($mode) {
    return $this->getWrapper() ? $this->getWrapper()->chmod($mode) : FALSE;
  }

  /**
   * Support for realpath().
   */
  public function realpath() {
    return $this->getWrapper() ? $this->getWrapper()->realpath($mode) : FALSE;
  }

  /**
   * Support for fopen(), file_get_contents(), file_put_contents() etc.
   *
   * @param string $uri
   *   A string containing the URI to the file to open.
   * @param string $mode
   *   The file mode ("r", "wb" etc.).
   * @param int $options
   *   A bit mask of STREAM_USE_PATH and STREAM_REPORT_ERRORS.
   * @param string $opened_path
   *   A string containing the path actually opened.
   *
   * @return bool
   *   Returns TRUE if file was opened successfully.
   *
   * @see http://php.net/manual/streamwrapper.stream-open.php
   */
  public function stream_open($uri, $mode, $options, &$opened_path) {
    $this->setUri($uri);
    $this->handle = fopen($this->getExternalUrl(), $mode);
    return (bool) $this->handle;
  }

  /**
   * Support for flock().
   *
   * @param int $operation
   *   One of the following:
   *   - LOCK_SH to acquire a shared lock (reader).
   *   - LOCK_EX to acquire an exclusive lock (writer).
   *   - LOCK_UN to release a lock (shared or exclusive).
   *   - LOCK_NB if you don't want flock() to block while locking (not
   *     supported on Windows).
   *
   * @return bool
   *   Always returns TRUE at the present time.
   *
   * @see http://php.net/manual/streamwrapper.stream-lock.php
   */
  public function stream_lock($operation) {
    if (in_array($operation, array(LOCK_SH, LOCK_EX, LOCK_UN, LOCK_NB))) {
      return flock($this->handle, $operation);
    }

    return TRUE;
  }

  /**
   * Support for fread(), file_get_contents() etc.
   *
   * @param int $count
   *   Maximum number of bytes to be read.
   *
   * @return string
   *   The string that was read, or FALSE in case of an error.
   *
   * @see http://php.net/manual/streamwrapper.stream-read.php
   */
  public function stream_read($count) {
    return fread($this->handle, $count);
  }

  /**
   * Support for fwrite(), file_put_contents() etc.
   *
   * @param string $data
   *   The string to be written.
   *
   * @return int
   *   The number of bytes written (integer).
   *
   * @see http://php.net/manual/streamwrapper.stream-write.php
   */
  public function stream_write($data) {
    return fwrite($this->handle, $data);
  }

  /**
   * Support for feof().
   *
   * @return bool
   *   TRUE if end-of-file has been reached.
   *
   * @see http://php.net/manual/streamwrapper.stream-eof.php
   */
  public function stream_eof() {
    return feof($this->handle);
  }

  /**
   * Support for fseek().
   *
   * @param int $offset
   *   The byte offset to got to.
   * @param int $whence
   *   SEEK_SET, SEEK_CUR, or SEEK_END.
   *
   * @return bool
   *   TRUE on success.
   *
   * @see http://php.net/manual/streamwrapper.stream-seek.php
   */
  public function stream_seek($offset, $whence) {
    // fseek returns 0 on success and -1 on a failure.
    // stream_seek   1 on success and  0 on a failure.
    return !fseek($this->handle, $offset, $whence);
  }

  /**
   * Support for fflush().
   *
   * @return bool
   *   TRUE if data was successfully stored (or there was no data to store).
   *
   * @see http://php.net/manual/streamwrapper.stream-flush.php
   */
  public function stream_flush() {
    return fflush($this->handle);
  }

  /**
   * Support for ftell().
   *
   * @return int
   *   The current offset in bytes from the beginning of file.
   *
   * @see http://php.net/manual/streamwrapper.stream-tell.php
   */
  public function stream_tell() {
    return ftell($this->handle);
  }

  /**
   * Support for fstat().
   *
   * @return array|bool
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   *
   * @see http://php.net/manual/streamwrapper.stream-stat.php
   */
  public function stream_stat() {
    return fstat($this->handle);
  }

  /**
   * Support for fclose().
   *
   * @return bool
   *   TRUE if stream was successfully closed.
   *
   * @see http://php.net/manual/streamwrapper.stream-close.php
   */
  public function stream_close() {
    return fclose($this->handle);
  }

  /**
   * Support for unlink().
   *
   * @param string $uri
   *   A string containing the URI to the resource to delete.
   *
   * @return bool
   *   TRUE if resource was successfully deleted.
   *
   * @see http://php.net/manual/streamwrapper.unlink.php
   */
  public function unlink($uri) {
    $this->setUri($uri);
    return unlink($this->getExternalUrl());
  }

  /**
   * No support for rename().
   *
   * @see http://php.net/manual/streamwrapper.rename.php
   */
  public function rename($from_uri, $to_uri) {
    return FALSE;
  }

  /**
   * Support for dirname().
   *
   * @see drupal_dirname()
   */
  public function dirname($uri = NULL) {
    $this->ensureUri($uri);
    return $wrapper = $this->getWrapperByUri($uri) ? $wrapper->dirname($uri) : FALSE;
  }

  /**
   * Support for mkdir().
   *
   * @param string $uri
   *   A string containing the URI to the directory to create.
   * @param int $mode
   *   Permission flags - see mkdir().
   * @param int $options
   *   A bit mask of STREAM_REPORT_ERRORS and STREAM_MKDIR_RECURSIVE.
   *
   * @return bool
   *   TRUE if directory was successfully created.
   *
   * @see http://php.net/manual/streamwrapper.mkdir.php
   */
  public function mkdir($uri, $mode, $options) {
    $this->setUri($uri);
    return $options & STREAM_REPORT_ERRORS ? mkdir($this->getExternalUrl(), $mode, $options & STREAM_MKDIR_RECURSIVE) : @mkdir($this->getExternalUrl(), $mode, $options & STREAM_MKDIR_RECURSIVE);
  }

  /**
   * Support for rmdir().
   *
   * @param string $uri
   *   A string containing the URI to the directory to delete.
   * @param int $options
   *   A bit mask of STREAM_REPORT_ERRORS.
   *
   * @return bool
   *   TRUE if directory was successfully removed.
   *
   * @see http://php.net/manual/streamwrapper.rmdir.php
   */
  public function rmdir($uri, $options) {
    $this->setUri($uri);
    return rmdir($this->getExternalUrl(), $options & STREAM_MKDIR_RECURSIVE);
  }

  /**
   * Support for stat().
   *
   * @param string $uri
   *   A string containing the URI to get information about.
   * @param int $flags
   *   A bit mask of STREAM_URL_STAT_LINK and STREAM_URL_STAT_QUIET.
   *
   * @return array|bool
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   *
   * @see http://php.net/manual/streamwrapper.url-stat.php
   */
  public function url_stat($uri, $flags) {
    $this->setUri($uri);
    $path = $this->getExternalUrl();
    // Suppress warnings if requested or if the file or directory does not
    // exist. This is consistent with PHP's plain filesystem stream wrapper.
    if ($flags & STREAM_URL_STAT_QUIET || !file_exists($path)) {
      return @stat($path);
    }
    else {
      return stat($path);
    }
  }

  /**
   * Support for opendir().
   *
   * @param string $uri
   *   A string containing the URI to the directory to open.
   * @param mixed $options
   *   Unknown (parameter is not documented in PHP Manual).
   *
   * @return bool
   *   TRUE on success.
   *
   * @see http://php.net/manual/streamwrapper.dir-opendir.php
   */
  public function dir_opendir($uri, $options) {
    $this->setUri($uri);
    $this->handle = opendir($this->getExternalUrl());

    return (bool) $this->handle;
  }

  /**
   * Support for readdir().
   *
   * @return string|bool
   *   The next filename, or FALSE if there are no more files in the directory.
   *
   * @see http://php.net/manual/streamwrapper.dir-readdir.php
   */
  public function dir_readdir() {
    return readdir($this->handle);
  }

  /**
   * Support for rewinddir().
   *
   * @return bool
   *   TRUE on success.
   *
   * @see http://php.net/manual/streamwrapper.dir-rewinddir.php
   */
  public function dir_rewinddir() {
    rewinddir($this->handle);
    // We do not really have a way to signal a failure as rewinddir() does not
    // have a return value and there is no way to read a directory handler
    // without advancing to the next file.
    return TRUE;
  }

  /**
   * Support for closedir().
   *
   * @return bool
   *   TRUE on success.
   *
   * @see http://php.net/manual/streamwrapper.dir-closedir.php
   */
  public function dir_closedir() {
    closedir($this->handle);
    // We do not really have a way to signal a failure as closedir() does not
    // have a return value.
    return TRUE;
  }

  /**
   * Generate external URL.
   *
   * @param string $uri
   *   Source URI.
   *
   * @return string
   *   External URL.
   */
  protected static function generateExternalUrl($uri) {
    // Acquire profile, entity_type, id and language from url.
    $url = parse_url($uri);

    $profile_name = isset($url['host']) ? trim($url['host'], '/') : NULL;
    $path = isset($url['path']) ? trim($url['path'], '/') : '';
    $parts = explode('/', $path);
    $entity_type = isset($parts[0]) ? $parts[0] : NULL;
    $entity_id = isset($parts[1]) ? $parts[1] : NULL;
    parse_str(isset($url['query']) ? $url['query'] : '', $options);

    // Make sure we have it all, otherwise bail!
    if (!$profile_name || !$entity_type || !$entity_id) {
      return FALSE;
    }

    $profile = entity_stream_wrapper_profile_load($profile_name);
    if (!$profile || !empty($profile->disabled)) {
      return FALSE;
    }

    // Default language is current.
    global $language;
    $options += [
      'lang' => $language->language,
      'https' => empty($GLOBALS['is_https']) ? 0 : 1,
    ];

    // Check for cached result.
    $url_key = "$profile_name//$entity_type/$entity_id?lang=" . $options['lang'] . '&' . 'https=' . $options['https'];
    if (!empty($profile->settings['cache']) && $profile->settings['cache'] == ENTITY_STREAM_WRAPPER_CACHE_PROFILE) {
      if ($cache = cache_get("entity_stream_wrapper:$url_key")) {
        return $cache->data;
      }
    }

    // No cache, let's find the correct result.
    $result = FALSE;
    if (!entity_get_info($entity_type)) {
      return $result;
    }
    $result = entity_stream_wrapper_get_mapped_url($profile, $entity_id, $entity_type, $options, $uri);
    if (!empty($profile->settings['cache']) && $profile->settings['cache'] == ENTITY_STREAM_WRAPPER_CACHE_PROFILE) {
      cache_set("entity_stream_wrapper:$url_key", $result);
    }
    return $result;
  }

}
