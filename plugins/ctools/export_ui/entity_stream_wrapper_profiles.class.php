<?php

/**
 * @file
 * Export-ui handler for the entity stream wrapper module.
 */

class entity_stream_wrapper_profiles extends ctools_export_ui {
  /**
   * Add configuration info to header.
   */
  public function list_table_header() {
    $header = parent::list_table_header();
    $operation[] = array_pop($header);
    $operation[] = array_pop($header);
    $header[] = array('data' => t('Plugins'), 'class' => array('ctools-export-ui-plugins'));
    $header[] = array('data' => t('Entities/bundles'), 'class' => array('ctools-export-ui-entities'));
    $header[] = array_pop($operation);
    $header[] = array_pop($operation);
    return $header;
  }

  /**
   * Add configuration info to row.
   */
  public function list_build_row($item, &$form_state, $operations) {
    parent::list_build_row($item, $form_state, $operations);
    $info = ['plugins' => [], 'entities' => []];

    if (!empty($item->settings['mappers'])) {
      $info = _entity_stream_wrapper_generate_plugins_entities_info($item->settings['mappers']);
    }

    $name = $item->{$this->plugin['export']['key']};
    $row = &$this->rows[$name];
    $operation[] = array_pop($row['data']);
    $operation[] = array_pop($row['data']);
    $row['data'][] = [
      'data' => $info['plugins'] ? join(', ', $info['plugins']) : '<em>' . t('None') . '</em>',
      'class' => ['ctools-export-ui-plugins'],
    ];
    $row['data'][] = [
      'data' => $info['entities'] ? join('<br>', $info['entities']) : '<em>' . t('None') . '</em>',
      'class' => ['ctools-export-ui-entities'],
    ];
    $row['data'][] = array_pop($operation);
    $row['data'][] = array_pop($operation);
  }

  /**
   * Page callback for the mappers page.
   */
  function mappers_page($js, $input, $item) {
    if (!isset($form_state['input']['form_id'])) {
      $form_state['input']['form_id'] = 'entity_stream_wrappers_profile_mappers_form';
    }

    // If we do any form rendering, it's to completely replace a form on the
    // page, so don't let it force our ids to change.
    if ($js && isset($_POST['ajax_html_ids'])) {
      unset($_POST['ajax_html_ids']);
    }

    $form = drupal_get_form('entity_stream_wrappers_profile_mappers_form', $item);
    $form = drupal_render($form);

    if (!$js) {
      drupal_set_title($this->get_page_title('mappers', $item));
      ctools_add_css('export-ui-list');
      return $form;
    }

    $commands = array();
    $commands[] = ajax_command_replace('#entity-stream-wrappers-profile-mappers-form', $form);
    print ajax_render($commands);
    ajax_footer();
  }

  /**
   * Page callback for the mappers add page.
   */
  function mappers_add_page($js, $input, $profile) {
    drupal_set_title($this->get_page_title('mappers_add', $profile));
    return drupal_get_form('entity_stream_wrappers_profile_mappers_edit_form', $profile);
  }

  /**
   * Page callback for the mappers add page.
   */
  function mappers_edit_page($js, $input, $profile, $mapper_id) {
    drupal_set_title($this->get_page_title('mappers_edit', $profile));
    return drupal_get_form('entity_stream_wrappers_profile_mappers_edit_form', $profile, $mapper_id);
  }

  /**
   * Page callback for the mappers add page.
   */
  function mappers_delete_page($js, $input, $profile, $mapper_id) {
    drupal_set_title($this->get_page_title('mappers_delete', $profile));
    return drupal_get_form('entity_stream_wrappers_profile_mappers_delete_confirm_form', $profile, $mapper_id);
  }

  /**
   * Page callback for the mappers state page.
   */
  function mappers_state_page($js, $input, $item, $mapper_id, $disabled) {
    if (isset($item->settings['mappers'][$mapper_id])) {
      $item->settings['mappers'][$mapper_id]['disabled'] = $disabled;
      entity_stream_wrapper_profile_save($item);
    }

    if (!$js) {
      drupal_goto(ctools_export_ui_plugin_base_path($this->plugin));
    }
    else {
      return $this->mappers_page($js, $input, $item);
    }
  }

}
