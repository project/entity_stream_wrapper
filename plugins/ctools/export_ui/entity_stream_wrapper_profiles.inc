<?php
/**
 * @file
 * Export UI integration.
 */

/**
 * Plugin definition for Ctools Export UI.
 */
$plugin = [
  'schema' => 'entity_stream_wrapper_profile',
  'access' => 'administer entity stream wrappers',
  'menu' => [
    'menu prefix' => 'admin/structure',
    'menu item' => 'entity-stream-wrapper',
    'menu title' => 'Entity stream wrappers',
    'menu description' => 'Configure entity stream wrappers in the system',
    'items' => [
      'mappers' => [
        'path' => 'list/%ctools_export_ui/mappers',
        'title' => 'Mappers',
        'page callback' => 'ctools_export_ui_switcher_page',
        'page arguments' => ['entity_stream_wrapper_profiles', 'mappers', 4],
        'load arguments' => ['entity_stream_wrapper_profiles'],
        'access arguments' => ['administer entity stream wrappers'],
        'type' => MENU_LOCAL_TASK,
        'weight' => -2,
      ],
      'mappers_add' => [
        'path' => 'list/%ctools_export_ui/mappers/add',
        'title' => 'Add',
        'page callback' => 'ctools_export_ui_switcher_page',
        'page arguments' => ['entity_stream_wrapper_profiles', 'mappers_edit', 4, FALSE],
        'load arguments' => ['entity_stream_wrapper_profiles'],
        'access arguments' => ['administer entity stream wrappers'],
        'type' => MENU_LOCAL_ACTION,
        'weight' => 0,
      ],
      'mappers_edit' => [
        'path' => 'list/%ctools_export_ui/mappers/%entity_stream_wrapper_mapper/edit',
        'tab_parent' => 'admin/structure/entity-stream-wrapper/list/%ctools_export_ui/mappers',
        'title' => 'Edit',
        'page callback' => 'ctools_export_ui_switcher_page',
        'page arguments' => ['entity_stream_wrapper_profiles', 'mappers_edit', 4, 6],
        'load arguments' => ['entity_stream_wrapper_profiles'],
        'access arguments' => ['administer entity stream wrappers'],
        'type' => MENU_LOCAL_TASK,
        'weight' => 0,
      ],
      'mappers_delete' => [
        'path' => 'list/%ctools_export_ui/mappers/%entity_stream_wrapper_mapper/delete',
        'tab_parent' => 'admin/structure/entity-stream-wrapper/list/%ctools_export_ui/mappers',
        'title' => 'Delete',
        'page callback' => 'ctools_export_ui_switcher_page',
        'page arguments' => ['entity_stream_wrapper_profiles', 'mappers_delete', 4, 6],
        'load arguments' => ['entity_stream_wrapper_profiles'],
        'access arguments' => ['administer entity stream wrappers'],
        'type' => MENU_LOCAL_TASK,
        'weight' => 1,
      ],
      'mappers_enable' => [
        'path' => 'list/%ctools_export_ui/mappers/%/enable',
        'title' => 'Enable',
        'page callback' => 'ctools_export_ui_switcher_page',
        'page arguments' => ['entity_stream_wrapper_profiles', 'mappers_state', 4, 6, FALSE],
        'load arguments' => ['entity_stream_wrapper_profiles'],
        'access arguments' => ['administer entity stream wrappers'],
        'type' => MENU_CALLBACK,
        'weight' => 2,
      ],
      'mappers_disable' => [
        'path' => 'list/%ctools_export_ui/mappers/%/disable',
        'title' => 'Disable',
        'page callback' => 'ctools_export_ui_switcher_page',
        'page arguments' => ['entity_stream_wrapper_profiles', 'mappers_state', 4, 6, TRUE],
        'load arguments' => ['entity_stream_wrapper_profiles'],
        'access arguments' => ['administer entity stream wrappers'],
        'type' => MENU_CALLBACK,
        'weight' => 2,
      ],
    ],
  ],

  // Add our custom operations.
  'allowed operations' => [
    'mappers'  => ['title' => t('Edit mappers')],
  ],

  'handler' => [
    'class' => 'entity_stream_wrapper_profiles',
    'parent' => 'ctools_export_ui',
  ],

  'title singular' => t('entity stream wrapper'),
  'title plural' => t('entity stream wrappers'),
  'title singular proper' => t('Entity stream wrapper'),
  'title plural proper' => t('Entity stream wrappers'),
];

/**
 * Settings form for handling extra profile settings.
 */
function entity_stream_wrapper_profiles_form(&$form, &$form_state) {
  $item = $form_state['item'];
  $form['expose_as_property'] = [
    '#type' => 'checkbox',
    '#title' => t('Expose profile as property'),
    '#description' => t('When checked, the profile will be exposed as two properties, one containing the raw entity:// url, and one containing the mapped url.'),
    '#default_value' => isset($item->settings['expose_as_property']) ? $item->settings['expose_as_property'] : TRUE,
  ];
  $form['cache'] = [
    '#type' => 'select',
    '#title' => t('Cache URL mapping'),
    '#description' => t('Select the level of caching for the URL mapping.'),
    '#options' => [
      ENTITY_STREAM_WRAPPER_CACHE_NONE => t('None'),
      ENTITY_STREAM_WRAPPER_CACHE_PROFILE => t('Per profile'),
      ENTITY_STREAM_WRAPPER_CACHE_MAPPER => t('Per mapper'),
    ],
    '#default_value' => isset($item->settings['cache']) ? $item->settings['cache'] : ENTITY_STREAM_WRAPPER_CACHE_PROFILE,
  ];
}

/**
 * Submit handler for handling extra profile settings.
 */
function entity_stream_wrapper_profiles_form_submit(&$form, &$form_state) {
  $form_state['item']->settings['expose_as_property'] = $form_state['values']['expose_as_property'];
  $form_state['item']->settings['cache'] = $form_state['values']['cache'];
}
