<?php
/**
 * @file
 * Plugin configuration for an alias url mapper
 */

$plugin = array(
  'title' => t('Alias'),
  'description' => t('Generate url alias for an entity'),
  'defaults' => [
    'protocol' => '',
  ]
);

/**
 * @return arrayList of available protocols.
 */
function _entity_stream_wrapper_url_mapper_alias_available_protocols() {
  return [
    '' => t('Auto'),
    'http' => t('Force HTTP'),
    'https' => t('Force HTTPS'),
  ];
}

/**
 * Settings form.
 */
function entity_stream_wrapper_url_mapper_alias_settings_form($settings, $form, $form_state) {
  $settings_form = [];
  $protocols = _entity_stream_wrapper_url_mapper_alias_available_protocols();
  $settings_form['protocol'] = [
    '#title' => t('Protocol'),
    '#type' => 'select',
    '#options' => $protocols,
    '#default_value' => $settings['protocol'],
  ];
  return $settings_form;
}

/**
 * Settings summary.
 */
function entity_stream_wrapper_url_mapper_alias_settings_summary($settings) {
  $protocols = _entity_stream_wrapper_url_mapper_alias_available_protocols();
  return t('Using protocol: %protocol', [
    '%protocol' => $protocols[$settings['protocol']],
  ]);
}

/**
 * Create url callback.
 */
function entity_stream_wrapper_url_mapper_alias_create_url($entity_type, $entity, $settings, $langcode, $uri) {
  $url = entity_uri($entity_type, $entity);
  $options = [
    'absolute' => TRUE,
  ];
  $list = language_list();
  if (!empty($list[$langcode])) {
    $options['language'] = $list[$langcode];
  }
  $result = url($url['path'], $options);
  if ($settings['protocol']) {
    if ($settings['protocol'] === 'https') {
      $result = str_replace('http://', 'https://', $result);
    }
    if ($settings['protocol'] === 'http') {
      $result = str_replace('https://', 'http://', $result);
    }
  }
  return $result;
}
