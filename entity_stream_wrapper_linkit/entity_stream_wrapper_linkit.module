<?php

/**
 * Implements hook_linkit_plugin_entities_alter().
 *
 * Override Linkit search entity plugins, so that we can provide an
 * entity stream wrapper profile option.
 */
function entity_stream_wrapper_linkit_linkit_search_plugin_entities_alter(&$plugins) {
  $overrides = [
    'LinkitSearchPluginNode' => 'EntityStreamWrapperLinkitSearchPluginNode',
    'LinkitSearchPluginFile' => 'EntityStreamWrapperLinkitSearchPluginFile',
    'LinkitSearchPluginUser' => 'EntityStreamWrapperLinkitSearchPluginUser',
    'LinkitSearchPluginTaxonomy_term' => 'EntityStreamWrapperLinkitSearchPluginTaxonomy_term',
    'LinkitSearchPluginEntity' => 'EntityStreamWrapperLinkitSearchPluginEntity',
  ];
  foreach ($plugins as $name => &$plugin) {
    if (isset($plugin['handler']['class'])) {
      if (isset($overrides[$plugin['handler']['class']])) {
        $plugin['handler']['class'] = $overrides[$plugin['handler']['class']];
      }
    }
  }
}

/**
 * Standard form for selecting an entity stream wrapper profile.
 */
function entity_stream_wrapper_linkit_linkit_settings_form($conf) {
  $profiles = entity_stream_wrapper_profile_option_list();
  return [
    '#title' => t('Entity stream wrapper profile'),
    '#type' => 'select',
    '#options' => $profiles,
    '#default_value' => isset($conf['entity_stream_wrapper_profile']) ? $conf['entity_stream_wrapper_profile'] : 0,
    '#description' => t('Use an entity stream wrapper profile for generating the links.'),
    '#empty_value' => 0,
  ];
}

/**
 * Create a path for an entity.
 */
function _entity_stream_wrapper_linkit_create_path($entity, $plugin, $profile, $conf) {
  // Create the URI for the entity.
  $id = entity_id($plugin['entity_type'], $entity);

  $path = 'entity://' . $conf['entity_stream_wrapper_profile'] . '/' . $plugin['entity_type'] . '/' . $id;

  // This is considered an external url due to the mainvideo scheme.
  $options = array(
    'external' => TRUE,
  );
  // Handle multilingual sites.
  if (isset($entity->language) && $entity->language != LANGUAGE_NONE && drupal_multilingual() && language_negotiation_get_any(LOCALE_LANGUAGE_NEGOTIATION_URL)) {
    $languages = language_list('enabled');
    // Only use enabled languages.
    $languages = $languages[1];

    if ($languages && isset($languages[$entity->language])) {
      $options['language'] = $languages[$entity->language];
    }
  }

  // Process the uri with the insert plugin.
  $path = linkit_get_insert_plugin_processed_path($profile, $path, $options);
  return $path;
}
