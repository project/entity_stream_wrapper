<?php
/**
 * @file
 * Define Linkit node search plugin class.
 */

/**
 * Reprecents a Linkit node search plugin.
 */
class EntityStreamWrapperLinkitSearchPluginNode extends LinkitSearchPluginNode {
  /**
   * Create a uri for a node via an entity stream wrapper profile.
   *
   * @param $entity
   *   The entity to get the path from.
   *
   * @return string
   *   A string containing the path of the entity, NULL if the entity has no
   *   uri of its own.
   */
  function createPath($entity) {
    // Use original node if applicable.
    if (!empty($this->conf['tnid_based']) && !empty($entity->tnid) && $entity->nid <> $entity->tnid) {
      $entity = node_load($entity->tnid);
    }

    // Only create an entity stream wrapper url, if a profile is chosen.
    if (empty($this->conf['entity_stream_wrapper_profile'])) {
      return parent::createPath($entity);
    }

    return _entity_stream_wrapper_linkit_create_path($entity, $this->plugin, $this->profile, $this->conf);
  }

  /**
   * Overrides LinkitSearchPluginNode::buildSettingsForm().
   */
  function buildSettingsForm() {
    // Get the parent settings form.
    $form = parent::buildSettingsForm();

    // Translation set support.
    $form[$this->plugin['name']]['tnid_based'] = [
      '#title' => t('Always link to original node in translation set'),
      '#description' => t('Check this to always return the original node in the translation set'),
      '#type' => 'checkbox',
      '#default_value' => isset($this->conf['tnid_based']) ? $this->conf['tnid_based'] : FALSE,
    ];

    // Select an entity stream wrapper profile.
    $form[$this->plugin['name']]['entity_stream_wrapper_profile'] = entity_stream_wrapper_linkit_linkit_settings_form($this->conf);
    return $form;
  }
}
