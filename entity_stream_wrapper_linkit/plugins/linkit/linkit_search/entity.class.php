<?php
/**
 * @file
 * Define Linkit node search plugin class.
 */

/**
 * Reprecents a Linkit node search plugin.
 */
class EntityStreamWrapperLinkitSearchPluginEntity extends LinkitSearchPluginEntity {
  /**
   * Create a uri for an entity via an entity stream wrapper profile.
   *
   * @param $entity
   *   The entity to get the path from.
   *
   * @return string
   *   A string containing the path of the entity, NULL if the entity has no
   *   uri of its own.
   */
  function createPath($entity) {
    // Only create an entity stream wrapper url, if a profile is chosen.
    if (empty($this->conf['entity_stream_wrapper_profile'])) {
      return parent::createPath($entity);
    }

    return _entity_stream_wrapper_linkit_create_path($entity, $this->plugin, $this->profile, $this->conf);
  }

  /**
   * Overrides LinkitSearchPluginNode::buildSettingsForm().
   */
  function buildSettingsForm() {
    // Get the parent settings form.
    $form = parent::buildSettingsForm();

    // Select an entity stream wrapper profile.
    $form[$this->plugin['name']]['entity_stream_wrapper_profile'] = entity_stream_wrapper_linkit_linkit_settings_form($this->conf);
    return $form;
  }
}
